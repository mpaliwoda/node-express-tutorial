//setup
var express = require('express');
var app = express();

var mongoose = require('mongoose');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

//configuration
mongoose.connect('mongodb://localhost/todolist');
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({type:'application/vnd.api+json'}));
app.use(methodOverride());

//Todo model
var Todo = mongoose.model('Todo', {
    text : String,
    done : Boolean
});

app.listen(8080);

//'controllers'

//get all todos
app.get('/api/todos', function(req, res) {
    Todo.find(function(error, todos) {
        if(error) {
            res.send(error);
            return;
        }
        res.json(todos);
    });
});

//create todo
app.post('/api/todos', function(req, res) {
    Todo.create({
        text : req.body.text,
        done : false 
    }, function(error, todo) {
        if(error) {
            res.send(error);
            return;
        }
        Todo.find(function(error, todos) {
            if(error)
                res.send(error);
            res.json(todos);
        });
    });
});

//check/uncheck done
app.put('/api/todos/:todo_id', function(req, res) {
    Todo.findById(
        req.params.todo_id,
        function(error, todo) {
            Todo.update(
                { _id : req.params.todo_id },
                { done: !todo.done },
                function(error, done) {
                   Todo.find(function(error, todos) {
                        if(error) {
                            res.send(error);
                            return;
                        }
                        res.json(todos);
                    });
            });
        });
});

//delete todo
app.delete('/api/todos/:todo_id', function(req, res) {
    Todo.remove({
        _id : req.params.todo_id
    }, function(error, todo) {
        if(error) {
            res.send(error);
            return;
        }
        Todo.find(function(error, todos) {
            if(error)
                res.send(error);
            res.json(todos);
        });
    });
});

app.get('*', function(req, res) {
    res.sendFile('/public/index.html', { root: __dirname });
});
